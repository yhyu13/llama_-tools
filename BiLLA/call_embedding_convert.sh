#!/bin/sh

META_BASE_MODEL=/home/hangyu5/Documents/Hugging-Face/LLAMA_TOOLS/repo/llama-dl/7B/consolidated.00.pth
DELTA_MODEL=/media/hangyu5/Home/Documents/Hugging-Face/BiLLa-7B-SFT

python3 embedding_convert.py \
    --model_dir $DELTA_MODEL \
    --meta_llama_pth_file $META_BASE_MODEL