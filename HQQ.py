import os

from transformers import AutoTokenizer
from hqq.engine.hf import HQQModelForCausalLM, AutoTokenizer
from hqq.core.quantize import *
import numpy as np
import torch
import argparse
from lightning import seed_everything
import random

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--base_model_dir", type=str, required=True, help="Path to the base model directory")
    parser.add_argument("--save_model_dir", type=str, required=True, help="Path to the save model directory")
    parser.add_argument("--bits", type=int, default=4)
    parser.add_argument("--seed", type=int, default=0)
    args = parser.parse_args()
    
    seed_everything(args.seed)

    quant_config = BaseQuantizeConfig(
        nbits=args.bits,  # quantize model to 4-bit
        group_size=64, quant_zero=True, quant_scale=False
    )

    # load un-quantized model, the model will always be force loaded into cpu
    model = HQQModelForCausalLM.from_pretrained(
        args.base_model_dir, 
        device_map="cpu",
        low_cpu_mem_usage=True,
        torch_dtype="auto",
        )

    # quantize model, the examples should be list of dict whose keys can only be "input_ids" and "attention_mask" 
    # with value under torch.LongTensor type.
    model.quantize_model(quant_config=quant_config)

    # save quantized model using safetensors
    model.save_quantized(args.save_model_dir)

if __name__ == "__main__":
    import logging

    logging.basicConfig(
        format="%(asctime)s %(levelname)s [%(name)s] %(message)s", level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S"
    )

    main()
