#!/bin/bash
eval "$(conda shell.bash hook)"
conda activate llama_tool
#MODEL_NAME=Xwin-Math-7B-V1.0
MODEL_NAME=LMCocktail-10.7B-v1
BITS=4
BASE_MODEL_DIR=/media/hangyu5/Home/Documents/Hugging-Face/LM_cocktail/LMCocktail-10.7B-v1/
SAVE_MODEL_DIR=/media/hangyu5/Home/Documents/Hugging-Face/LM_cocktail/LMCocktail-10.7B-v1-HQQ-${BITS}bit/
TRANSFORMERS_VERBOSITY=debug CUDA_VISIBLE_DEVICES=0 python HQQ.py --base_model_dir $BASE_MODEL_DIR --save_model_dir $SAVE_MODEL_DIR --bits $BITS |& tee ./$MODEL_NAME-hqq.log