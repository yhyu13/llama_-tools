#!/bin/bash
eval "$(conda shell.bash hook)"
conda activate llama_tool
#MODEL_NAME=Xwin-Math-7B-V1.0
MODEL_NAME=ShareGPT4V-7B
BASE_MODEL_DIR=/media/hangyu5/Home/Documents/Hugging-Face/$MODEL_NAME/
SAVE_MODEL_DIR=/media/hangyu5/Home/Documents/Hugging-Face/$MODEL_NAME-GPTQ/
TRANSFORMERS_VERBOSITY=debug CUDA_VISIBLE_DEVICES=0 python AutoGPTQ.py --base_model_dir $BASE_MODEL_DIR --save_model_dir $SAVE_MODEL_DIR |& tee ./$MODEL_NAME-autogptq.log