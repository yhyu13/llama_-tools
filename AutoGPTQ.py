import os

from transformers import AutoTokenizer
from auto_gptq import AutoGPTQForCausalLM, BaseQuantizeConfig
import numpy as np
import torch
import argparse
from lightning import seed_everything
import random

def get_wikitext2(nsamples, seqlen, model):
    from datasets import load_dataset
    traindata = load_dataset('wikitext', 'wikitext-2-raw-v1', split='train')

    try:
        tokenizer = AutoTokenizer.from_pretrained(model, use_fast=False)
    except:
        tokenizer = AutoTokenizer.from_pretrained(model, use_fast=True)
    trainenc = tokenizer("\n\n".join(traindata['text']), return_tensors='pt')
    
    traindataset = []
    for _ in range(nsamples):
        i = random.randint(0, trainenc.input_ids.shape[1] - seqlen - 1)
        j = i + seqlen
        inp = trainenc.input_ids[:, i:j]
        attention_mask = torch.ones_like(inp)
        traindataset.append({'input_ids':inp,'attention_mask': attention_mask})
    return traindataset

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--base_model_dir", type=str, required=True, help="Path to the base model directory")
    parser.add_argument("--save_model_dir", type=str, required=True, help="Path to the save model directory")
    parser.add_argument("--bits", type=int, default=4)
    parser.add_argument("--dataset", type=str, default="wikitext2")
    parser.add_argument("--seed", type=int, default=0)
    args = parser.parse_args()
    
    seed_everything(args.seed)
    
    if args.dataset == "wikitext2":
        traindataset = get_wikitext2(128, 2048, args.base_model_dir)

    quantize_config = BaseQuantizeConfig(
        bits=4,  # quantize model to 4-bit
        group_size=128,  # it is recommended to set the value to 128
        desc_act=True,  # desc_act and group size only works on triton
    )

    # load un-quantized model, the model will always be force loaded into cpu
    model = AutoGPTQForCausalLM.from_pretrained(
        args.base_model_dir, 
        quantize_config,
        device_map="cpu",
        low_cpu_mem_usage=True,
        torch_dtype="auto",
        )

    # quantize model, the examples should be list of dict whose keys can only be "input_ids" and "attention_mask" 
    # with value under torch.LongTensor type.
    model.quantize(traindataset, use_triton=False)

    # save quantized model using safetensors
    model.save_quantized(args.save_model_dir, use_safetensors=True)

if __name__ == "__main__":
    import logging

    logging.basicConfig(
        format="%(asctime)s %(levelname)s [%(name)s] %(message)s", level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S"
    )

    main()
