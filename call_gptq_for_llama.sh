eval "$(conda shell.bash hook)"
conda activate llama_conv

MAX_SEQ_LEN=4096
MODEL_NAME=Xwin-Math-7B-V1.0
BASE_MODEL=/media/hangyu5/Home/Documents/Hugging-Face/$MODEL_NAME
TARGET_DIR=/media/hangyu5/Home/Documents/Hugging-Face/$MODEL_NAME-gptq-4bit
mkdir $TARGET_DIR
TARGET_MODEL=$TARGET_DIR/$MODEL_NAME-4bit-128g-actor.safetensors
ARGS="--wbits 4 --true-sequential --act-order --groupsize 128"
#ARGS="--wbits 4 --true-sequential --groupsize 128"

# Or save compressed `.safetensors` model
CUDA_VISIBLE_DEVICES=0,1 python ./repo/GPTQ-for-LLaMa/llama.py $BASE_MODEL c4 $ARGS --save_safetensors $TARGET_MODEL |& tee ./gptq.log

# Benchmark generating a MAX_SEQ_LEN token sequence with the saved model
CUDA_VISIBLE_DEVICES=0,1 python ./repo/GPTQ-for-LLaMa/llama.py $BASE_MODEL c4 $ARGS --load $TARGET_MODEL --benchmark $MAX_SEQ_LEN --check |& tee $TARGET_DIR/benchmark.txt