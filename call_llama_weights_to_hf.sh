#!/bin/sh

INPUT_DIR=/home/hangyu5/Documents/Hugging-Face/LLAMA_TOOLS/repo/llama-dl/
OUTPUT_DIR=/media/hangyu5/Home/Documents/Hugging-Face/llama-7B-hf/
MODEL_SIZE=7B

PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python
python convert_llama_weights_to_hf.py --input_dir $INPUT_DIR  --output_dir $OUTPUT_DIR --model_size $MODEL_SIZE