#!/bin/sh

BASE_MODEL=/media/hangyu5/Documents/Hugging-Face/llama-7B-hf
TARGET_DIR=/media/hangyu5/Documents/Hugging-Face/chimera-inst-chat-7b
DELTA_MODEL=/media/hangyu5/Documents/Hugging-Face/chimera-inst-chat-7b-delta

python apply_delta_stablevicuna.py --base $BASE_MODEL --target $TARGET_DIR --delta $DELTA_MODEL
